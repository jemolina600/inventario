INSERT INTO users VALUES (1, 'ivan','12345', 'ivan', 'agudelo', 'abivan@correo.com');
INSERT INTO users VALUES (2,'fabian','12345', 'fabian', 'rojas', 'fabian@correo.com');
INSERT INTO users VALUES (3,'yohon','12345', 'yohon', 'bravo', 'yohon@correo.com');
INSERT INTO users VALUES (4,'freddy','12345', 'freddy', 'sierra', 'freddy@correo.com');

INSERT INTO rols VALUES (1,'ROLE_USER');
INSERT INTO rols VALUES (2,'ROLE_ADMIN');

INSERT INTO user_has_rol VALUES (1,1), (1,2);
INSERT INTO user_has_rol VALUES (2,1);
INSERT INTO user_has_rol VALUES (3,2);

