package com.adsi.inventory.web.rest;

import com.adsi.inventory.service.IUserService;
import com.adsi.inventory.service.dto.UsersDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class UserResourse {

    @Autowired
    IUserService service;

    @GetMapping("/user")
    public Page<UsersDTO> getAll(@RequestParam(value = "page") Integer pageNumber,
                                 @RequestParam(value = "size") Integer pageSize){
        return service.getAll(pageNumber, pageSize);
    }

    @PostMapping("/user")
    public ResponseEntity<?> create(@RequestBody UsersDTO usersDTO){
        UsersDTO dto = null;
        Map<String, Object> response = new HashMap<>();
        try{
            dto = service.create(usersDTO);
        }catch (DataAccessException err){
            response.put("message", "Error en la base de datos");
            return new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);

        }
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UsersDTO> getById(@PathVariable Long id){
        return new ResponseEntity<>(service.getById(id),HttpStatus.OK);
    }
}
