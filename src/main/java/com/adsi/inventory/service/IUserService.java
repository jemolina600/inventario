package com.adsi.inventory.service;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.service.dto.UsersDTO;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

public interface IUserService {
    Page<UsersDTO> getAll(Integer pageNumber, Integer pageSize);

    UsersDTO create(UsersDTO usersDTO);


    UsersDTO getById(Long id);
}
